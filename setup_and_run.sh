#!/bin/bash

REPOS=(
    "git@gitlab.com:main2350726/front-vue.git"
    "git@gitlab.com:main2350726/back-laravel.git"
    "git@gitlab.com:main2350726/mariadb.git"
    "git@gitlab.com:main2350726/nginx.git"
)

clone_or_update_repo() {
    local repo_url=$1
    local repo_name=$(basename "$repo_url" .git)
    local repo_dir="$repo_name"

    if [ -d "$repo_dir" ]; then
        echo "Updating repository $repo_name..."
        git -C "$repo_dir" pull
    else
        echo "Cloning repository $repo_name..."
        git clone "$repo_url" "$repo_dir"
    fi
}

for repo in "${REPOS[@]}"; do
    clone_or_update_repo "$repo"
done

docker compose down --remove-orphans
docker compose up   -d --build

echo "It's Running .... Enjoy ... "
